# Docker-based Salt Master

A Docker-based salt-master and salt-api implementation, with pygit2 preinstalled.

## Kubernetes  Example

I run this container in a Kubernetes cluster, and my instance is self-salted with configuration.  Here's the salt recipe and kube configuration.  You'll want to configure a persistent volume for storage, which I've omitted from this configuration for simplicity.

### Kube config

	apiVersion: v1
	kind: Service
	metadata:
	  namespace: salt-master
	  name: salt-master-loadbalancer
	spec:
	  externalTrafficPolicy: Cluster
	  loadBalancerIP: 10.10.10.10
	  sessionAffinity: None
	  type: LoadBalancer
	  selector:
	    app: salt-master
	  ports:
	    - protocol: TCP
	      name: salt-master-zmq-4505
	      port: 4505
	      targetPort: 4505
	    - protocol: TCP
	      name: salt-master-zmq-4506
	      port: 4506
	      targetPort: 4506
	    - protocol: TCP
	      name: salt-master-api-5417
	      port: 5417
	      targetPort: 5417
	
	---
	
	apiVersion: apps/v1
	kind: Deployment
	metadata:
	  namespace: salt-master
	  labels:
	    app: salt-master
	  name: salt-master
	spec:
	  replicas: 1
	  selector:
	    matchLabels:
	      app: salt-master
	  revisionHistoryLimit: 10
	  template:
	    metadata:
	      labels:
	        app: salt-master
	    spec:
	      containers:
	      - image: base10/docker-salt-master:3001.1-1
	        imagePullPolicy: Always
	        name: salt-master
	        ports:
	        - containerPort: 4505
	          name: 4505tcp45053
	          protocol: TCP
	        - containerPort: 4506
	          name: 4506tcp45063
	          protocol: TCP
	        - containerPort: 5417
	          name: 5417tcp54173
	          protocol: TCP
	        volumeMounts:
	        - mountPath: /etc/salt/pki
	          name: salt-master-nfs-claim
	          subPath: pki
	        - mountPath: /etc/salt/master.d
	          name: salt-master-nfs-claim
	          subPath: master.d
	      dnsConfig:
	        options:
	        - name: ndots
	          value: "1"
	      dnsPolicy: ClusterFirst
	      restartPolicy: Always
	      terminationGracePeriodSeconds: 30
	      volumes:
	      - name: salt-master-nfs-claim
	        persistentVolumeClaim:
	          claimName: salt-master-nfs-claim

### Salt config

	this is a docker-based config, so we use the docker file path for our work
	{% set saltHome = '/etc/salt' %}
	  
	Salt root directory:
	  file.directory:
	    - name: {{ saltHome }}
	    - user: root
	    - group: root
	    
	Master configuration directory:
	  file.directory:
	    - name: {{ saltHome }}/master.d
	    - user: root
	    - group: root
	
	Deploy gitroot pubkey:
	file.managed:
	    - name: {{ saltHome }}/master.d/git-deploy.pub
	    - contents_pillar: docker:salt:master:ssh_pubkey
	    - user: root
	    - group: root
	    - mode: 644
	Deploy gitroot privkey:
	file.managed:
	    - name: {{ saltHome }}/master.d/git-deploy.key
	    - contents_pillar: docker:salt:master:ssh_key
	    - user: root
	    - group: root
	    - mode: 600
	Deploy gitroot conf:
	file.managed:
	    - name: {{ saltHome }}/master.d/gitroot.conf
	    - source: salt://{{ slspath }}/gitroot.conf
	    - user: root
	    - group: root
	    - mode: 600
	
	Deploy salt-api config:
	    file.managed:
	    - name: {{ saltHome }}/master.d/salt-api.conf
	    - source: salt://{{ slspath }}/salt-api.conf
	    - user: root
	    - group: root
	    - mode: 755


### Changelog
New in version 3003.1-1:

1. Upgrade to salt 3003.1
2. Reformat this readme

New in version 3002.2-1:

1. Upgrade to salt 3002.2

New in version 3001.3-1:

1. Upgrade to salt 3001.3

New in version 3001.1-2:

1. Upgrade to salt 3001.1
2. Switch to Debian Buster as our base platform
3. Switch to python3 since py2 has been dropped from support

New in version 3000.3-1:

1. Upgrade to salt 3000.3

New in version 3000.2-1:

1. Upgrade to salt 3000.2

New in version 3000.1-1:

1. Upgrade to salt 3000.1

New in version 3000.0-1:

1. Upgrade to salt 3000.0

New in version 2019.2.2-1:

1. Upgrade to salt 2019.2.2

New in version 2019.2.0-1:

1. Upgrade to salt 2019.2.0

New in version 2018.3.3-1:

1. Change versioning to match Salt version
2. Upgrade to salt 2018.3.3

New in version 0.7.0:

1, Upgrade to salt 2018.3.2

New in version 0.6.0:

1, Upgrade to Salt 2017.7.2

New in version 0.4.1:

1. Switch to Debian Stretch so we get pygit2 (needed for authenticated gitfs repos)

New in version 0.4.0:

1. Upgrade to salt 2016.3.2
2. Switched from Alpine (through Arch) to Debian Jessie. Hopefully that's the end of the base changes.
3. API now uses cherrypy since that's the Debian default
4. Run a minion locally so the salt master can manage itself