#!/bin/bash

apt-get update
apt-get install -y curl supervisor

curl -L https://bootstrap.saltstack.com -o /tmp/install_salt.sh
sh /tmp/install_salt.sh -P -M -X stable 3003.1

# add salt-api since it's not default, ssh client so we can use cloud, and pygit2 so we can use private git repos
# and virt-what since we are salting ourselves and this improves performance
apt-get install -y salt-api ssh-client python3-pygit2 virt-what

# set up self as salt master
echo "master: localhost" >> /etc/salt/minion
echo "id: self" >> /etc/salt/minion

# clean up logs and temp files, and self
apt-get clean
rm -rf /var/log/*
rm -rf /var/lib/apt/lists/*
rm /tmp/install_salt.sh /tmp/bootstrap.sh
