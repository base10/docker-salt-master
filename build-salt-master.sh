#!/bin/bash

TAG=$1
RELEASE=$2

if [ "" = "$TAG" ]; then
        echo Please provide a tag name as an argument.
else

        echo "Building tag $TAG"
        docker build --tag="base10/salt-master:$TAG"  https://bitbucket.org/base10/docker-salt-master.git#$TAG
        if [ "--release" = "$RELEASE" ]; then
            docker tag base10/salt-master:$TAG base10/salt-master:latest
            docker push base10/salt-master
        fi;
fi;
